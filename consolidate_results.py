def combine_results(participant):
	return [
		{'hr':participant['rest_bpm'],'mem':participant['rest_rate']},
		{'hr':participant['active_bpm'],'mem':participant['active_rate']}
	]

if __name__ == '__main__':
	import sys
	import pandas as pd
	from load_participants import get_participant_list

	participants = get_participant_list(sys.argv[1])

	combine_list = []
	for p in participants:
		combined = combine_results(p)
		combine_list.extend(combined)

	combine_frame = pd.DataFrame(combine_list)
	combine_frame.to_csv('combine_partic.csv')