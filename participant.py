hr_multiplier = 4#Because the hr was tested over a 15 second period

def Participant(age,rest_hr, active_hr, rest_data, active_data):
    rest_bpm = float(rest_hr * hr_multiplier)
    active_bpm = float(active_hr * hr_multiplier)

    rest_rate = __eval_morpheme_recall_data(rest_data)
    active_rate = __eval_morpheme_recall_data(active_data)

    return {
        'age': age,
        'rest_bpm': rest_bpm,
        'active_bpm': active_bpm,
        'rest_rate': rest_rate,
        'active_rate': active_rate
        }


def __eval_morpheme_recall_data(data_arr):
    recall_arr = data_arr['keyValueResponses']

    num_correct = 0
    total_asked = len(recall_arr)
    
    for key_pairing in recall_arr:
        correct_word = key_pairing['value']
        response = key_pairing['returnedResponse']
        if correct_word == response:
            num_correct = num_correct+1
    return num_correct/total_asked
