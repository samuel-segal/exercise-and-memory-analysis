
def create_delta_obj(participant):
	return {
		'd_hr': (participant['active_bpm']-participant['rest_bpm']),
		'd_mem': (participant['active_rate']-participant['rest_rate'])
	}


if __name__ == '__main__':
	import sys
	import pandas as pd
	from load_participants import get_participant_list

	participants = get_participant_list(sys.argv[1])

	delta_list = []
	for p in participants:
		delta_obj = create_delta_obj(p)
		delta_list.append(delta_obj)

	delta_frame = pd.DataFrame(delta_list)

	delta_frame.to_csv('delta_partic.csv')