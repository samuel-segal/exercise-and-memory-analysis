import csv
import json

from functools import lru_cache

from participant import Participant

@lru_cache(maxsize=2)
def get_participant_list(csv_file_name):

    out_list = list()
    
    with open(csv_file_name) as form_csv:
        csv_reader = csv.reader(form_csv)
        next(csv_reader)#Skips first line
        for row in csv_reader:
            experiment_data = row[1]
            age = row[2]
            
            json_obj = json.loads(experiment_data)

            arr = json_obj['morphemeRecallArr']
            

            participant = Participant(
                    age = age,
                    rest_hr = json_obj['restHeartBeat'],
                    active_hr = json_obj['exerciseHeartBeat'],
                    rest_data = arr[0],
                    active_data = arr[1]
                )
            out_list.append(participant)
    return out_list

if __name__ == "__main__":
    import sys
    participant_list = get_participant_list(sys.argv[1])
    print(participant_list)
