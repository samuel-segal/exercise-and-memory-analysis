import pandas as pd

from load_participants import get_participant_list

def save_participant_csv(file_name):
    participant_list = get_participant_list(file_name)
    data_frame = pd.DataFrame(participant_list)
    print(data_frame)
    data_frame.to_csv('participant.csv')


if __name__ == "__main__":
    import sys
    save_participant_csv(sys.argv[1])
